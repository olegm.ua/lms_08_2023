from django.contrib import admin

from students.models import Student


# admin.site.register(
#     [
#         Student,
#     ]
# )


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    ordering = ("first_name", "group_name")
    list_display = ("first_name", "last_name", "email", "age", "group_name")
    # list_display_links = (("first_name", "last_name"),)
    search_fields = ("first_name__istartswith",)
    # date_hierarchy = "birth_date"
    list_filter = ("birth_date", "group_name")
