import datetime
import os

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpRequest, Http404, HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (
    CreateView,
    UpdateView,
    TemplateView,
    ListView,
    DeleteView,
)

from config import settings
from groups.models import Group
from students.forms import StudentForm
from .models import Student


class IndexView(TemplateView):
    template_name = "index.html"
    extra_context = {"key": "guys", "site_name": "LMS"}

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["my_name"] = "Oleg"
        return context


class CreateStudentView(LoginRequiredMixin, CreateView):
    template_name = "students/create.html"
    form_class = StudentForm
    # model = Student
    # fields = ("first_name", "last_name", "email", "group_name")
    success_url = reverse_lazy("students:create_student")


class AllStudents(LoginRequiredMixin, ListView):
    model = Student
    template_name = "students/list.html"
    context_object_name = "students"


def get_students(request: HttpRequest, params):
    students = Student.objects.all()
    search_fields = ["first_name", "last_name", "email"]

    for name, value in params.items():
        if name == "search_fields":
            request.session[f"search_text_{datetime.datetime.now()}"] = value
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__iconains": value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{name: value})
    return render(
        request, template_name="students/list.html", context={"students": students}
    )


def search_history(request: HttpRequest):
    log_info = {}
    for key, value in request.session.items():
        if "search_text_" in key:
            log_info.update({key: value})
    return render(
        request, template_name="search_history.html", context={"search_logs": log_info}
    )


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    template_name = "students/update.html"
    # context_object_name = "students"
    form_class = StudentForm
    pk_url_kwarg = "uuid"
    queryset = Student.objects.all()
    success_url = reverse_lazy("students:students")


class DeleteStudent(LoginRequiredMixin, DeleteView):
    template_name = "students/delete.html"
    context_object_name = "students"
    form_class = StudentForm
    pk_url_kwarg = "uuid"
    queryset = Student.objects.filter()
    success_url = reverse_lazy("students:students")


class ResumeUploadView(ListView):
    model = Student
    template_name = "students/resume_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        resumes = []
        for student in self.object_list:
            if student.resume_file:
                resumes.append(student.resume_file)
        context["resumes"] = resumes
        return context


def download_resume(request, resume):
    file_path = os.path.join(settings.MEDIA_ROOT, resume.resume.name)
    if os.path.exists(file_path):
        with open(file_path, "rb") as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response["Content-Disposition"] = "inline; filename=" + os.path.basename(
                file_path
            )
            return response
    raise Http404


def testing(request: HttpRequest):
    # student_1 = Student.objects.get(uuid="1f27bdc9b7cc43dcaf55d49e885eaf76")
    # student_2 = Student.objects.filter(uuid="ea8099ce52664b508b51d53ec7450c86")
    # print(f"{student_1=}")
    # print(f"{student_2=}")
    # print(f"{student_1.group.group_name}")
    # target_group = Group.objects.first()
    # data_to_save = []
    # for _ in range(10):
    #     new_student = Student()
    #     new_student.first_name = "first_name"
    #     new_student.last_name = "last_name"
    #     new_student.email = "email@email.com"
    #     new_student.group = target_group
    #     data_to_save.append(new_student)
    #
    # Student.objects.bulk_create(data_to_save)

    target_group = Group.objects.filter(group_name__in=["Java"]).first()
    # students = target_group.all_students_in_group.all()
    return render(request, template_name="testing.html", context={"data": target_group})


# class ResumeUploadView(View):
#     template_name = "students/upload.html"
#
#     def get(self, request):
#         form = UploadFileForm()
#         return render(request, self.template_name, {"form": form})
#
#     def post(self, request):
#         form = UploadFileForm(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#             return redirect(
#                 "resume_list"
#             )  # Redirect to a view that displays all resumes
#         return render(request, self.template_name, {"form": form})
