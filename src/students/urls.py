from django.urls import path

import students
from .views import (
    IndexView,
    AllStudents,
    CreateStudentView,
    UpdateStudentView,
    DeleteStudent,
    ResumeUploadView,
    search_history,
)

app_name = "students"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("students/", AllStudents.as_view(), name="students"),
    path("students/create/", CreateStudentView.as_view(), name="create_student"),
    path(
        "students/update/<uuid:uuid>/",
        UpdateStudentView.as_view(),
        name="update_student",
    ),
    path(
        "students/delete/<uuid:uuid>/", DeleteStudent.as_view(), name="delete_student"
    ),
    path("students/testing/", students.views.testing, name="testing"),
    path("students/resume_list/", ResumeUploadView.as_view(), name="upload"),
    path("search-history/", search_history, name="search_history"),
]
