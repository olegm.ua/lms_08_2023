import datetime
import random
from uuid import uuid4

from django.db import models

# from django.utils.translation import gettext_lazy as _
from faker import Faker

from base_app.models import Person


class Student(Person):
    uuid = models.UUIDField(
        default=uuid4, primary_key=True, editable=False, unique=True, db_index=True
    )

    group = models.ForeignKey(
        to="groups.Group",
        null=True,
        related_name="all_students_in_group",
        on_delete=models.CASCADE,
    )
    avatar = models.ImageField(
        upload_to="static/students/avatars",
        default=None,
        blank=True,
        null=True,
    )

    resume_file = models.FileField(
        upload_to="resumes/", default=None, blank=True, null=True
    )

    class Meta:
        verbose_name = "Student"
        verbose_name_plural = "Students"
        ordering = ("last_name",)

    def age(self):
        return datetime.datetime.now().year - self.birth_date.year

    def __str__(self):
        return f"{self.first_name} {self.last_name}, email: {self.email}"

    @classmethod
    def generate_instances(cls, count):
        fake = Faker()
        for _ in range(count):
            cls.objects.create(
                last_name=fake.last_name(),
                first_name=fake.first_name(),
                email=fake.email(),
                birth_date=fake.date_time_between(start_date="-30y", end_date="-18y"),
                group_name=random.choice(["Python", "Java", "QA"]),
            )


# class UploadedFile(models.Model):
#     resume_file = models.FileField(upload_to="resumes/")
#     avatar = models.ImageField(upload_to="students/avatars")


#     uploaded_at = models.DateTimeField(auto_now_add=True)
