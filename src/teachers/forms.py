from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm

from .models import Teacher


class TeacherForm(ModelForm):
    class Meta:
        model = Teacher
        fields = ("first_name", "last_name", "email", "group_name")

    @staticmethod
    def normalize_text(text: str):
        return text.strip().capitalize()

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@mail.ru" in email.lower():
            raise ValidationError("Mail.ru is forbidden!")
        return email

    # def clean_experience(self):
    #     experience = self.cleaned_data["experience"]
    #
    #     if experience <= 1:
    #         raise ValidationError("Your experience is not sufficient")
    #     return experience

    def clean_last_name(self):
        return self.normalize_text(self.cleaned_data["last_name"])

    def clean_first_name(self):
        return self.normalize_text(self.cleaned_data["first_name"])

    def clean(self):
        cleaned_data = super().clean()

        first_name = cleaned_data["first_name"]
        last_name = cleaned_data["last_name"]

        if first_name == last_name:
            raise ValidationError("First_name and last_name cant be equal!")
        return cleaned_data


class UploadFileForm(forms.Form):
    avatar = forms.ImageField()
    resume = forms.FileField()
