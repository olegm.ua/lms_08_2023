from django.contrib import admin  # NOQA: 401

from teachers.models import Teacher


# admin.site.register(Teacher)


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    ordering = ("last_name",)
    list_display = ("first_name", "last_name", "email", "group_name")
    search_fields = ("first_name__istartswith",)
    list_filter = ("first_name", "group_name")
