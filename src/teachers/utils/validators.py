from django.core.exceptions import ValidationError


def first_name_validator(first_name: str):
    if len(first_name) < 2:
        raise ValidationError("The name must not be abbreviated")
