from django.urls import path

from teachers.views import (
    AllTeachers,
    CreateTeacherView,
    UpdateTeacherView,
    DeleteTeacher,
    IndexView,
)
from . import views

app_name = "teachers"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("teachers/", AllTeachers.as_view(), name="teachers"),
    path("teachers/create/", CreateTeacherView.as_view(), name="create_teacher"),
    path(
        "teachers/update/<uuid:uuid>/",
        UpdateTeacherView.as_view(),
        name="update_teacher",
    ),
    path(
        "teachers/delete/<uuid:uuid>/", DeleteTeacher.as_view(), name="delete_teacher"
    ),
    # path("teachers/group/", TeacherListView.as_view(), name="group_teacher"),
    path("teachers/group/", views.group_teacher, name="group_teacher"),
    path("teachers/testing/", views.testing, name="testing"),
]
