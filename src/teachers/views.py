from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpRequest
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
    TemplateView,
)

from groups.models import Group
from .forms import TeacherForm
from .models import Teacher


class IndexView(TemplateView):
    template_name = "index.html"
    extra_context = {"key": "guys", "site_name": "LMS"}

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["my_name"] = "Oleg"
        return context


# def index(request):
#     return render(request, template_name="index.html")


class AllTeachers(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = "teachers/list.html"
    context_object_name = "teachers"


class CreateTeacherView(LoginRequiredMixin, CreateView):
    template_name = "teachers/create.html"
    form_class = TeacherForm
    model = Teacher
    # fields = ("first_name", "last_name", "email", "group_name")
    success_url = reverse_lazy("teachers:create_teacher")


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    template_name = "teachers/update.html"
    context_object_name = "teachers"
    form_class = TeacherForm
    pk_url_kwarg = "uuid"
    queryset = Teacher.objects.filter()
    success_url = reverse_lazy("teachers:teachers")


class DeleteTeacher(LoginRequiredMixin, DeleteView):
    template_name = "teachers/delete.html"
    context_object_name = "teachers"
    form_class = TeacherForm
    pk_url_kwarg = "uuid"
    queryset = Teacher.objects.filter()
    success_url = reverse_lazy("teachers:teachers")


# class TeacherListView(View):
#     template_name = "teachers/group.html"
#
#     def get(self, request):
#         groups = Group.objects.filter()
#         return render(request, self.template_name, {"groups": groups})


def group_teacher(request):
    return render(
        request,
        template_name="teachers/group.html",
        context={"data": Group.objects.all()[1]},
    )


def testing(request: HttpRequest):
    Teacher.objects.all().delete()
    target_group = Group.objects.filter(group_name__in=["Python"]).first()
    teachers = target_group.all_teachers_in_group.all()
    print(type(teachers))
    return render(request, template_name="testing.html", context={"data": target_group})
