import random
from datetime import datetime
from uuid import uuid4

from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker

from students.utils.validators import first_name_validator


class Teacher(models.Model):
    first_name = models.CharField(
        max_length=120,
        blank=True,
        null=True,
        validators=[MinLengthValidator(1), first_name_validator],
    )
    last_name = models.CharField(max_length=120, blank=True, null=True)

    email = models.EmailField(max_length=155, null=True)
    start_date = models.DateField(null=True)
    group_name = models.CharField(max_length=50, null=True)
    birth_date = models.DateField(datetime, null=True)
    uuid = models.UUIDField(default=uuid4, editable=False, null=True, db_index=True)

    group = models.ManyToManyField(
        to="groups.Group",
        related_name="all_teachers_in_group",
    )
    avatar = models.ImageField(
        upload_to="static/teachers/avatars",
        default=None,
        blank=True,
        null=True,
        verbose_name="Avatar",
    )

    def __str__(self):
        return f"{self.first_name} {self.last_name}, email: {self.email}, uuid: {self.uuid}, group: {self.group_name}"

    @classmethod
    def generate_instances(cls, count):
        fake = Faker()
        for _ in range(count):
            Teacher.objects.create(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                email=fake.email(),
                start_date=fake.date_this_year(),
                group_name=random.choice(["Python", "Java", "QA"]),
                birth_date=fake.date_time_between(start_date="-30y", end_date="-18y"),
            )
