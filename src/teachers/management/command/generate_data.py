from django.core.management.base import BaseCommand
from teachers.models import Teacher


class Command(BaseCommand):
    help = "This will create other teachers"

    def add_arguments(self, parser):
        parser.add_argument("number_of_teachers", type=int)

    def handle(self, *args, **options):
        number = options.get("number_of_teachers")
        Teacher.generate_instances(5)
        print(f"Created {number} teachers", Teacher.objects.all(), sep="\n")
