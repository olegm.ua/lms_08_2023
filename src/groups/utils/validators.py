from django.core.exceptions import ValidationError


def name_of_curator_validator(name_of_curator: str):
    if "vova" in name_of_curator.lower():
        raise ValidationError("Vova is not correct, should be Volodymyr")
