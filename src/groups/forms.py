from django.core.exceptions import ValidationError
from django.forms import ModelForm

from .models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = (
            "name_of_curator",
            "name_of_teacher",
            "quantity_of_students",
            "group_name",
        )

    @staticmethod
    def normalize_text(text: str):
        return text.strip().capitalize()

    def clean_name_of_curator(self):
        return self.normalize_text(self.cleaned_data["name_of_curator"])

    def clean_name_of_teacher(self):
        return self.normalize_text(self.cleaned_data["name_of_teacher"])

    def clean(self):
        cleaned_data = super().clean()

        name_of_curator = cleaned_data["name_of_curator"]
        name_of_teacher = cleaned_data["name_of_teacher"]

        if name_of_curator == name_of_teacher:
            raise ValidationError("Curator can't be teacher!")
        return cleaned_data
