from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import (
    TemplateView,
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
)

from .forms import GroupForm
from .models import Group


class IndexView(TemplateView):
    template_name = "index.html"
    extra_context = {"key": "guys", "site_name": "LMS"}

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["my_name"] = "Oleg"
        return context


class AllGroups(LoginRequiredMixin, ListView):
    model = Group
    template_name = "groups/list.html"
    context_object_name = "groups"


class CreateGroupView(LoginRequiredMixin, CreateView):
    template_name = "groups/create.html"
    form_class = GroupForm
    model = Group
    # fields = ("first_name", "last_name", "email", "group_name")
    success_url = reverse_lazy("students:create_student")


class UpdateGroupView(LoginRequiredMixin, UpdateView):
    template_name = "groups/update.html"
    context_object_name = "groups"
    form_class = GroupForm
    pk_url_kwarg = "pk"
    queryset = Group.objects.filter()
    success_url = reverse_lazy("groups: groups")


class DeleteGroup(LoginRequiredMixin, DeleteView):
    template_name = "groups/delete.html"
    context_object_name = "groups"
    form_class = GroupForm
    pk_url_kwarg = "pk"
    queryset = Group.objects.filter()
    success_url = reverse_lazy("groups:groups")
