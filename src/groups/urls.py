from django.urls import path

from .views import IndexView, AllGroups, UpdateGroupView, DeleteGroup, CreateGroupView

app_name = "groups"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("groups/", AllGroups.as_view(), name="groups"),
    path("groups/create/", CreateGroupView.as_view(), name="create_group"),
    path("groups/update/<int:pk>/", UpdateGroupView.as_view(), name="update_group"),
    path("groups/delete/<int:pk>/", DeleteGroup.as_view(), name="delete_group"),
]
