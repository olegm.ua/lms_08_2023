from django.contrib import admin

from groups.models import Group


# admin.site.register(Group)
@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    ordering = ("group_name",)
    list_display = (
        "name_of_teacher",
        "group_name",
        "start_date",
        "name_of_curator",
    )
    search_fields = ("name_of_teacher__istartswith",)
    list_filter = ("group_name", "name_of_teacher")
