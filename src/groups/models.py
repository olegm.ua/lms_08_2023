import random

from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker

from groups.utils.validators import name_of_curator_validator


# from students.models import Person


class Group(models.Model):
    name_of_curator = models.CharField(
        max_length=120,
        blank=True,
        null=True,
        validators=[MinLengthValidator(1), name_of_curator_validator],
    )
    name_of_teacher = models.CharField(max_length=120)
    group_name = models.CharField(max_length=50)
    quantity_of_students = models.PositiveSmallIntegerField(
        default=20, null=True, blank=True
    )
    start_date = models.DateField(null=True)

    def __str__(self):
        return (
            f"Куратор:{self.name_of_curator} викладач: {self.name_of_teacher}, \n"
            f"дата початку - {self.start_date}, спеціалізація - {self.group_name}"
        )

    @classmethod
    def generate_instances(cls, count):
        fake = Faker()
        for _ in range(count):
            list_of_specialities = ["Python", "Java", "QA"]
            Group.objects.create(
                name_of_curator=fake.name(),
                name_of_teacher=fake.name(),
                group_name=random.choice(list_of_specialities),
                quantity_of_students=random.randint(10, 15),
                start_date=fake.date_this_year(),
            )
