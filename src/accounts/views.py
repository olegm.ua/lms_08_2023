from datetime import datetime

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.http import HttpResponse, HttpRequest
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views import generic
from django.views.generic import CreateView, RedirectView, TemplateView

from accounts.forms import UserRegistrationForm, UserEditForm, ProfileEditForm
from accounts.services.emails import send_registration_email
from accounts.utils.token_generator import TokenGenerator


class IndexView(TemplateView):
    template_name = "index.html"
    extra_context = {"site_name": "LMS"}

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        print(f"In my view! Current time: {self.request.current_time}")
        context["my_name"] = "Oleg"
        return context


class SignUpView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"


class UserRegistration(CreateView):
    template_name = "registration/create_user.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(request=self.request, user_instance=self.object)
        return super().form_valid(form)


class PhoneSignupView(AnonymousRequiredMixin, FormView):
    """Display the register form and handle user registration."""

    form_class = UserRegistrationForm
    template_name = "registration/signup.html"
    success_url = reverse_lazy("phone_auth:phone_login")

    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(PhoneSignupView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.save()
        if form.errors:
            return render(self.request, self.template_name, context={"form": form})
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class ActivateUserView(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data!!!")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)

            return super().get(request, *args, **kwargs)
        return HttpResponse("Wrong data!!!")


def send_email(request: HttpRequest) -> HttpResponse:
    current_time = datetime.now().strftime("%d.%m.%Y %H:%M")
    send_mail(
        subject="Hello from LMS",
        message=f"Маловік Олег, {current_time}",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[
            settings.EMAIL_HOST_USER,
            "mikolaz2727@gmail.com",
            "olegm.ua@gmail.com",
        ],
        fail_silently=False,
    )

    return HttpResponse("Done!!!")


@login_required
class EditProfileView(generic.UpdateView):
    model = User
    form_class = UserEditForm
    profile_form_class = ProfileEditForm
    template_name = "registration/edit.html"
    success_url = reverse_lazy("index")

    def get_object(self, queryset=None):
        return self.request.user

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["data"] = self.request.POST or None
        kwargs["files"] = self.request.FILES or None
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["profile_form"] = self.profile_form_class(
            instance=self.request.user.profile,
            data=self.request.POST or None,
            files=self.request.FILES or None,
        )
        return context

    def form_valid(self, form):
        profile_form = self.get_context_data()["profile_form"]
        if profile_form.is_valid():
            self.object = form.save()
            profile_form.save()
            messages.success(self.request, "Profile updated successfully")
            return super().form_valid(form)
        else:
            messages.error(self.request, "Error updating your profile")
            return self.form_invalid(form)


def user_testing(request):
    user = get_user_model()()
    user.set_password("456789")
    user.username = "user_for_test"
    user.save()
