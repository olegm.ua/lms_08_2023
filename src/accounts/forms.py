from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

# from phone_auth.forms import PhoneAuthForm
from phonenumber_field.formfields import PhoneNumberField

from accounts.models import Profile


class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        widgets = {
            "phone_number": PhoneNumberPrefixWidget(),
        }
        fields = ("phone_number", "password1", "password2", "first_name", "last_name")

    def clean(self):
        _clean_data = super().clean()

        if not bool(self.cleaned_data["email"]) and not bool(
            self.cleaned_data["phone_number"]
        ):
            raise ValidationError(
                "Insert email or phone number. At least one of them should be set."
            )

        elif not bool(self.cleaned_data["email"]):
            if (
                get_user_model()
                .objects.filter(phone_number=_clean_data["phone_number"])
                .exists()
            ):
                raise ValidationError("User with phone number already exists!!!")

        elif not bool(self.cleaned_data["phone_number"]):
            if get_user_model().objects.filter(email=_clean_data["email"]).exists():
                raise ValidationError("User with email already exists!!!")

        else:
            ...


class UserEditForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        widgets = {
            "phone_number": PhoneNumberPrefixWidget(),
        }
        fields = ["first_name", "last_name", "email", "phone_number"]

    def clean_email(self):
        data = self.cleaned_data["email"]
        qs = User.objects.exclude(id=self.instance.id).filter(email=data)
        if qs.exists():
            raise forms.ValidationError("Email already in use.")
        return data


class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ["user_type", "avatar"]


class ProfileForm(forms.Form):
    class Meta:
        model = Profile
        fields = ("email", "password", "phone_number", "image")
        widgets = {"password": forms.PasswordInput()}

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ("email", "password1", "password2", "first_name", "last_name")

    def clean(self):
        _clean_data = super().clean()

        if not bool(self.cleaned_data["email"]) and not bool(
            self.cleaned_data["phone_number"]
        ):
            raise ValidationError(
                "Insert email or phone number. At least one of them should be set."
            )

        elif not bool(self.cleaned_data["email"]):
            if (
                get_user_model()
                .objects.filter(phone_number=_clean_data["phone_number"])
                .exists()
            ):
                raise ValidationError("User with phone number already exists!!!")

        elif not bool(self.cleaned_data["phone_number"]):
            if get_user_model().objects.filter(email=_clean_data["email"]).exists():
                raise ValidationError("User with email already exists!!!")

        else:
            ...


class PhoneForm(forms.Form):
    number = PhoneNumberField(region="UA")
