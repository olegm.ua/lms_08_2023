from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser

# from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import User
from django.db import models
from django.db.models import IntegerField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from location_field.models.plain import PlainLocationField
from phonenumber_field.modelfields import PhoneNumberField

from accounts.managers import CustomerManager


class Customer(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("Name"), max_length=150, blank=True)
    last_name = models.CharField(_("Surname"), max_length=150, blank=True)
    phone_number = IntegerField(
        unique=True,
        verbose_name="Phone Number",
        blank=False,
        help_text="Enter 13 digits phone number",
    )

    USERNAME_FIELD = "phone_number"
    REQUIRED_FIELDS = []

    objects = CustomerManager()

    email = models.EmailField(_("email address"), null=True, blank=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    department = models.CharField(
        _("department"), max_length=200, null=True, blank=True
    )
    birth_date = models.DateTimeField(_("birth_date"), null=True, blank=True)
    avatar = models.ImageField(
        _("avatar"), upload_to="media/avatars", null=True, blank=True
    )

    objects = CustomerManager()

    USERNAME_FIELD = "phone_number"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("customer")
        verbose_name_plural = _("customers")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    # def email_user(self, subject, message, from_email=None, **kwargs):
    #     """Send an email to this user."""
    #     send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_period_of_registration(self):
        return f"Time on site: {timezone.now() - self.date_joined}"


class Profile(models.Model):
    USERS_TYPE = (("teacher", "Teacher"), ("student", "Student"), ("mentor", "Mentor"))
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    avatar = models.ImageField(
        default="profile_pics/default.jpeg", upload_to="profile_pics"
    )
    email = models.EmailField(default="email address", unique=True)
    city = models.CharField(max_length=255)
    phone_number = PhoneNumberField()
    location = PlainLocationField(based_fields=["city"], zoom=7)
    user_type = models.CharField(
        choices=USERS_TYPE, max_length=30, null=True, blank=True
    )

    def __str__(self):
        return f"{self.user.email} Profile"


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
