from django.core.validators import MinLengthValidator
from django.db import models

from students.utils.validators import first_name_validator


class Person(models.Model):
    first_name = models.CharField(
        max_length=120,
        blank=True,
        null=True,
        validators=[MinLengthValidator(1), first_name_validator],
    )
    last_name = models.CharField(max_length=120, blank=True, null=True)
    email = models.EmailField(max_length=155)
    group_name = models.CharField(max_length=50, null=True)
    birth_date = models.DateField(null=True)

    class Meta:
        abstract = True


# class UserProfile(models.Model):
#     user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
#     photo = models.ImageField(upload_to="media/img/profiles")
#     phone_number = models.CharField(max_length=16)
#     birthday = models.DateField(blank=True, null=True)
#     city = models.CharField(max_length=255)
