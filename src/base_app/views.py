from django.views.generic import TemplateView


class IndexView(TemplateView):
    template_name = "index.html"
    extra_context = {"site_name": "LMS"}

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["my_name"] = "Oleg"
        return context


class Custom404View(TemplateView):
    template_name = "404.html"
