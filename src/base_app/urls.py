from django.urls import path

from .views import IndexView, Custom404View

app_name = "base_app"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    # path("registration/login/", UserRegistration.as_view(), name="login"),
    # path("registration/reset/", UserRegistration.as_view(), name="reset"),
    # path("registration/registration/", UserRegistration.as_view(), name="registration"),
]

handler404 = Custom404View.as_view()
